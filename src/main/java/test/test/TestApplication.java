package test.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

@SpringBootApplication
public class TestApplication {

    public static void main(String[] args) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        SpringApplication.run(TestApplication.class, args);

        String fileBase = "DreamTeamKey";
//        generateNewKeys(fileBase);

        //-------------------------------------------------------------------

        String d = "4&Прогулочно-познавательный маршрут «Заборное»&savc@bk.ru&+7 (977) 896-74-11&3&2019-11-05&2019-11-22";

        PublicKey publicKey = getPublic(fileBase);
        Cipher encryptCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] cipherText = encryptCipher.doFinal(d.getBytes(UTF_8));
        String encString = Base64.getEncoder().encodeToString(cipherText);
        System.out.println(encString);

        PrivateKey privateKey = getPrivate(fileBase);
        Cipher decryptCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);

        byte[] bytes = Base64.getDecoder().decode(encString);
        String result = new String(decryptCipher.doFinal(bytes), UTF_8);
        System.out.println(result);
    }

    private static void generateNewKeys(String fileBase) throws NoSuchAlgorithmException, IOException {

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair kp = kpg.generateKeyPair();

        PublicKey publicKey = kp.getPublic();
        PrivateKey privateKey = kp.getPrivate();

        try (FileOutputStream out = new FileOutputStream(fileBase + ".key")) {
            out.write(kp.getPrivate().getEncoded());
        }

        try (FileOutputStream out = new FileOutputStream(fileBase + ".pub")) {
            out.write(kp.getPublic().getEncoded());
        }
    }

    private static PublicKey getPublic(String fileBase) throws InvalidKeySpecException, IOException, NoSuchAlgorithmException {
        byte[] bytes = Files.readAllBytes(Paths.get(fileBase + ".pub"));
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(keySpec);
    }

    private static PrivateKey getPrivate(String fileBase) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] bytes = Files.readAllBytes(Paths.get(fileBase + ".key"));
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePrivate(keySpec);
    }

}
